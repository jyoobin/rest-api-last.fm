#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 30 02:31:47 2018

@author: yoobin
"""

import requests
import webbrowser

api_key = 'ad3e669d510dc55301bde2680fac218b'
api_sec = '296c8345d1af2f401d82fb4656a7d036'
token =   'zLgEzSOTBi1FnqCig88tE40iyH4V-TYc'
url = 'http://ws.audioscrobbler.com/2.0/'
    
def similar_artists(name):
    payload = {'method':'artist.getinfo','autocorrect':[1],'artist':name, 'api_key':api_key, 'format':'json'}
    r = requests.get(url,payload)
    d = r.json()
    gen = (el['name'] for el in d['artist']['similar']['artist'])
    for artist in gen: print artist


def track_info(track, artist = ""):
    infolist = []
    payload = {'method':'track.search','track':track, 'artist':artist, 'api_key':api_key, 'format':'json'}
    r = requests.get(url,payload)
    d = r.json()
    vinfo = d['results']['trackmatches']['track']               # vinfo is a list of results
    for info in vinfo:
        infolist.append(info)
    for index, info in zip(range(1,21), vinfo): 
        print index, '.', info['name'], ' - ' , info['artist']
    cindex = input("Enter the index of the song : ")
    try:
        webbrowser.open(infolist[cindex-1]['url'])
    except:
        print "Cannot find a link"
        
    
    

def top_tracks(name):
    infolist = []
    payload = {'method':'artist.gettoptracks','artist':name, "api_key": api_key, "format":"json"}
    r = requests.get(url,payload)
    d = r.json()
    gen = (el for el in d['toptracks']['track'])
    print name+"'s Best Track List: " 
    for index, track in zip(range(1,21),gen): 
        print index, '.', track['name']
        infolist.append(track)
    cindex = input("Enter the index of the song : ")
    try:
        webbrowser.open(infolist[cindex-1]['url'])
    except:
        print "Cannot find a link"


def add_tag(track, artist):
    tag = raw_input("Enter the tag: ")
    payload = {'method':'track.addTags', 'artist':artist, 'tags':tag , 'api_key':api_key, 'sk':api_sec}
    r = requests.post(url, payload)
#   print r.text
    
    
name = raw_input("Enter the name of an artist: ")
print similar_artists(name)

name = raw_input("Enter the name of an artist: ")
print top_tracks(name)

track = raw_input("Enter the name of a track: ")
print track_info(track)


#print add_tag("as we enter", )